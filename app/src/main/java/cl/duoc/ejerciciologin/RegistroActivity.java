package cl.duoc.ejerciciologin;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import cl.duoc.ejerciciologin.bd.almacenamiento;
import cl.duoc.ejerciciologin.entidades.Registro;

public class RegistroActivity extends AppCompatActivity {
    private EditText etUsuario, etNombres, etApellidos, etRut, etFechaNacimiento, etClave, etClave2;
    private Button btnRegistro;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        etUsuario=(EditText) findViewById(R.id.etUsuarioRegistro);
        etNombres=(EditText) findViewById(R.id.etNombres);
        etApellidos=(EditText) findViewById(R.id.etApellidos);
        etRut=(EditText) findViewById(R.id.etRut);
        etFechaNacimiento=(EditText) findViewById(R.id.etFechaNacimiento);
        etClave=(EditText) findViewById(R.id.etClave);
        etClave2=(EditText) findViewById(R.id.etRepetirClave);
        btnRegistro=(Button) findViewById(R.id.btnRegistro);

        btnRegistro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String mensajeError="";
                if(etUsuario.getText().toString().length()<1){
                    mensajeError+="Ingrese usuario \n";
                }
                if (etNombres.getText().toString().length()<1){
                    mensajeError+="Ingrese sus nombres \n";
                }
                if (etApellidos.getText().toString().length()<1){
                    mensajeError+="Ingrese sus apellidos \n";
                }
                if(!isValidarRut(etRut.getText().toString())){
                    mensajeError+="Ingrese un rut valido \n";
                }
                if(etClave.getText().toString().length()<1){
                    mensajeError+="Ingrese contraseña \n";
                }
                else{
                    if (etClave2.getText().toString().length()<1){
                        mensajeError+="Repita contraseña \n";
                    }
                    else {
                        if(!etClave.getText().toString().equals(etClave2.getText().toString())){
                            mensajeError+="Contraseñas no coinciden";
                        }
                    }
                }

                if(mensajeError.equals("")){
                    Registro nuevoRegistro=new Registro();
                    nuevoRegistro.setUsuario(etUsuario.getText().toString());
                    nuevoRegistro.setNombres(etNombres.getText().toString());
                    nuevoRegistro.setApellidos(etApellidos.getText().toString());
                    nuevoRegistro.setRut(etRut.getText().toString());
                    nuevoRegistro.setFechaNacimiento(etFechaNacimiento.getText().toString());
                    nuevoRegistro.setClave(etClave.getText().toString());

                    almacenamiento.agregarRegistro(nuevoRegistro);
                    Toast.makeText(RegistroActivity.this, "Usuario registrado correctamente", Toast.LENGTH_SHORT).show();
                }



            }
            public  boolean isValidarRut(String rut) {

                boolean validacion = false;
                try {
                    rut =  rut.toUpperCase();
                    rut = rut.replace(".", "");
                    rut = rut.replace("-", "");
                    int rutAux = Integer.parseInt(rut.substring(0, rut.length() - 1));

                    char dv = rut.charAt(rut.length() - 1);

                    int m = 0, s = 1;
                    for (; rutAux != 0; rutAux /= 10) {
                        s = (s + rutAux % 10 * (9 - m++ % 6)) % 11;
                    }
                    if (dv == (char) (s != 0 ? s + 47 : 75)) {
                        validacion = true;
                    }

                } catch (java.lang.NumberFormatException e) {
                } catch (Exception e) {
                }
                return validacion;
            }
        });

    }
}
